qemu-img create -f qcow2 /data01/VMs-path-cy6/cy6-ovs.qcow2 100G
virt-install \
	--name cy6-r-compute  \
	--ram 10240 \
	--disk path=/data01/VMs-path-cy6/cy6-ovs.qcow2 \
	--vcpus 3 \
	--os-type linux \
	--os-variant rhel7 \
	--network bridge=br0 \
	--graphics none \
	--console pty,target_type=serial \
	--location '/data01/VMs-path-cy2/CentOS-7-x86_64-DVD-1804.iso'\
	--extra-args 'console=ttyS0,115200n8 serial'
