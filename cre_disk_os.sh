#!/bin/sh


##
VMNAME="cy01-ceph"
#VMNAME="cy01-ceph"
DIR="/os-volume-data/${VMNAME}"
mkdir $DIR
## OSD VM NUM
#for i in 1
#for i in 001 002 003 004 005 006
#for i in 011 012 013
#for i in 001
#for i in 003 004 005
for i in 231 232 233 241 242 243
do 

## DISK NAME
	for dn in  b c d e
#	for dn in  b c d e f g h 
	do
	qemu-img create -f qcow2 ${DIR}/${VMNAME}${i}vd${dn}.img 20G
	virsh attach-disk --persistent ${VMNAME}${i}   --subdriver qcow2  --source ${DIR}/${VMNAME}${i}vd${dn}.img --target vd${dn}
	done
done

#virsh attach-disk ceph-osd01 --source /data01/libvirt_img/ceph-osd01-vdd.img --target vdd 

