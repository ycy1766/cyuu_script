virsh attach-disk ceph-osd01 --subdriver qcow2  --source  /data01/libvirt_img/osd01-vdf.img --target vdf

virsh attach-disk ceph-osd02 --subdriver qcow2  --source  /data01/libvirt_img/osd02-vdf.img --target vdf

virsh attach-disk ceph-osd03 --subdriver qcow2  --source  /data01/libvirt_img/osd03-vdf.img --target vdf
