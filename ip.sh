
cat << EOF > /etc/sysconfig/network-scripts/ifcfg-eth1
TYPE=Ethernet
BOOTPROTO=none
NAME=eth1
DEVICE=eth1
ONBOOT=yes
IPADDR=192.168.93.$IP
GATEWAY=192.168.0.1
PREFIX=16

EOF

 systemctl restart network

 ping -c 3 8.8.8.8
:
