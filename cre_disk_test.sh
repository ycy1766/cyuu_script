#!/bin/sh


DIR="/data01/libvirt_img_test"
## OSD VM NUM
for i in 1 2 3
do 

## DISK NAME
	for dn in b c
	do
	qemu-img create -f qcow2 ${DIR}/test_ceph-osd0${i}-vd${dn}.img 20G
	virsh attach-disk test_ceph-osd0${i}   --subdriver qcow2  --source ${DIR}/test_ceph-osd0${i}-vd${dn}.img --target vd${dn}
	done
done

#virsh attach-disk ceph-osd01 --source /data01/libvirt_img/ceph-osd01-vdd.img --target vdd 

