#!/bin/sh


DIR="/data01/libvirt_img_cy"
## OSD VM NUM
for i in 1 2 3
do 

## DISK NAME
	for dn in b c d
	do
	#qemu-img create -f qcow2 ${DIR}/osd0${i}-vd${dn}.img 20G
	qemu-img create -f qcow2 ${DIR}/cy-ceph-osd0${i}-vd${dn}.img 20G
	#virsh attach-disk ceph-osd0${i}   --subdriver qcow2  --source ${DIR}/osd0${i}-vd${dn}.img --target vd${dn}
	virsh attach-disk cy-ceph-osd0${i}   --subdriver qcow2  --source ${DIR}/cy-ceph-osd0${i}-vd${dn}.img --target vd${dn}
	done
done

#virsh attach-disk ceph-osd01 --source /data01/libvirt_img/ceph-osd01-vdd.img --target vdd 

