#!/bin/sh


IP=2

cat << EOF > /etc/sysconfig/network-scripts/ifcfg-eth1
TYPE=Ethernet
BOOTPROTO=none
NAME=eth1
DEVICE=eth1
ONBOOT=yes
IPADDR=192.168.93.$IP
GATEWAY=192.168.0.1
PREFIX=16
EOF

systemctl restart network

yum install -y epel*
yum install -y ansible git python-pip

ssh-keygen -f /root/.ssh/id_rsa -q -N  ""


echo "        StrictHostKeyChecking no" >> /etc/ssh/ssh_config
systemctl restart sshd

cd /opt
git clone https://gitlab.com/ycy1766/linux_nic_set.git
