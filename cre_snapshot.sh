#!/bin/sh

LANG=C
STATUS="run"
SNAPSHOT_NUM=1
DES="default"

virsh list  | grep ${STATUS} |\
grep -v cy5  |  \
awk  -v SNAPSHOT_NUM=${SNAPSHOT_NUM} -v DES=${DES} '{print  \
"virsh snapshot-create-as --domain " $2 \
" --name snapshot" SNAPSHOT_NUM  \
" --description " DES } ' | sh
