#!/bin/sh

LANG=C
STATUS="run"
SNAPSHOT_NUM=4
virsh  list --all | grep cy5-r-co | awk  -v SNAPSHOT_NUM=${SNAPSHOT_NUM} -v DES=${DES} '{print  \
"virsh snapshot-revert --domain " $2 \
"  --snapshotname snapshot" SNAPSHOT_NUM  } '\
#| sh
