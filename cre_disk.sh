#!/bin/sh


##
VMNAME="cy02-ceph"
#VMNAME="cy01-ceph"
DIR="/data/volume-img/${VMNAME}"
mkdir $DIR
## OSD VM NUM
#for i in 1
#for i in 001 002 003 004 005 006
#for i in 011 012 013
for i in  161 162 163
#for i in 003 004 005
#for i in 171 172 173 181 182 183
do 

## DISK NAME
	#for dn in  h
	#for dn in  f g h
	for dn in   b c d e   
	do
	qemu-img create -f qcow2 ${DIR}/${VMNAME}${i}vd${dn}.img 20G
	virsh attach-disk --persistent ${VMNAME}${i}   --subdriver qcow2  --source ${DIR}/${VMNAME}${i}vd${dn}.img --target vd${dn}
	done
done

#virsh attach-disk ceph-osd01 --source /data01/libvirt_img/ceph-osd01-vdd.img --target vdd 

