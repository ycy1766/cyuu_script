#!/bin/sh

LANG=C

virsh list | grep run| awk '{print "virsh destroy " $2}' | sh
STATUS="off"
virsh list --all | grep ${STATUS} | awk '{print "virsh undefine " $2}'  | sh
